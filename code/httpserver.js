#!/usr/bin/env node

var http = require('http');

// Callback-Funktion zur Verarbeitung des HTTP-Requests
// s. http://javascriptissexy.com/understand-javascript-callback-functions-and-use-them
// Parameter
// request (in): Das HTTP-Request-Objekt
// response (out): Das Antwort-Objekt
var requestHandler = function(request, response) {
	console.log('Request erhalten: ', request.url);
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hallo, Welt.\n');
}

http.createServer(requestHandler).listen(8888, 'localhost');

console.log('Server ist unter der Adresse http://localhost:8888/ zu erreichen.');
