#!/usr/bin/env node
// Berechnung von Fibonacci-Zahlen
// https://de.wikipedia.org/wiki/Fibonacci-Folge
// Parameter
// n (in): Die Zahl, zu der die Fibonacci-Zahl errechnet werden soll
// Rueckgabe: Die Fibonacci-Zahl von n
var fibonacci = function(n) {
	if(n < 1) { return 0;}
	else if(n == 1 || n == 2) { return 1;}
	else if(n > 2) { return fibonacci(n - 1) + fibonacci(n - 2);}
};

// Berechnung von Fibonacci-Zahlen nach der Binet-Formel
// https://de.wikipedia.org/wiki/Goldener_Schnitt#Zusammenhang_mit_den_Fibonacci-Zahlen 
// Parameter
// n (in): Die Zahl, zu der die Fibonacci-Zahl errechnet werden soll
// Rueckgabe: Die Fibonacci-Zahl von n
var fibonacci2 = function(n) {
	var phi = (1 + Math.sqrt(5))/2;
	return Math.round((Math.pow(phi, n) - Math.pow(1-phi, n))/Math.sqrt(5));
};

// Findet die ersten K Fibonacci-Zahlen
// Parameter
// k (in): Anzahl der Fibonacci-Zahlen
// Rueckgabe: Ein Feld mit dem Ergebnis
var firstkfib = function(k) {
	var i = 1;
	var arr = [];
	for(i = 1; i < k+1; i++) {
		arr.push(fibonacci(i));
	}
	return arr;
};

// Ausgabe eines Feldes auf dem Bildschirm
// Parameter
// arr (in): Das Feld
var fmt = function(arr) {
	return arr.join(" ");
};

var k = 20;
console.log("firstkfib(" + k + ")");
console.log(fmt(firstkfib(k)));

